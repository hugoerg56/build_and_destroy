class StartScene < MG::Scene
  include SceneMethods
  
  def initialize
    EM.cancel_timer($timeout_timer) if $timeout_timer
    @buttons = []
    GameHelper.add_background("main-bg.png", self)
    add_logo
    add_menu_buttons
    
    on_touch_begin do |touch|
      check_menu(touch)
    end
  end
  
  def add_logo
    logo = MG::Sprite.new(ImageHelper.name_for_phone("logo.png"))
    logo.position = [$visible_size.width / 2, ImageHelper.top_porcent(35)]
    add logo, 1
  end  
  
  def add_menu_buttons
    add_start_button
    add_options_button
    add_shop_button
  end  
  
  def add_start_button
    start_button = MG::Sprite.new(ImageHelper.name_for_phone("button-start.png"))
    start_button.position = [$visible_size.width / 2, ImageHelper.top_porcent(69.5)]
    add start_button, 1
    @buttons << [start_button, "OpponentScene-GC"]
  end  
  
  def add_options_button
    option_button = MG::Sprite.new(ImageHelper.name_for_phone("button-options.png"))
    option_button.position = [$visible_size.width / 2, ImageHelper.top_porcent(82)]
    add option_button, 1
    @buttons << [option_button, "OptionsScene"]    
  end  
  
  def add_shop_button
    shop_button = MG::Sprite.new(ImageHelper.name_for_phone("button-shop.png"))
    shop_button.position = [$visible_size.width / 2, ImageHelper.top_porcent(94.5)]
    add shop_button, 1
    @buttons << [shop_button, "ShopScene"]    
  end

  
end