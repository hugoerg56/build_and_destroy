class SupportScene < MG::Scene
  include SceneMethods
  
  def initialize
    @buttons = []    
    GameHelper.add_background("main-bg.png", self)
    @buttons << GameHelper.add_back_button_to("OptionsScene-Clean", self)

    title = MG::Text.new('-- SUPPORT --', 'OpenSans-Bold', 46)
    title.position = [$visible_size.width / 2, ($visible_size.height / 2) + 60]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title    
    
    title = MG::Text.new('WORKING ON THIS SCREEN', 'OpenSans-Bold', 46)
    title.position = [$visible_size.width / 2, $visible_size.height / 2]
    title.text_color = [1.0,1.0,1.0,1.0]
    add title   

    on_touch_begin do |touch|
      check_menu(touch)
    end        
  end

end