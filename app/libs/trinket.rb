class Trinket
  attr_accessor :name
  attr_accessor :price
  attr_accessor :image
  attr_accessor :image_icon
   
  def initialize(name, price)
    self.name = name
    self.price = price
    self.image = name.downcase.gsub(" ", "_") + ".png"
    self.image_icon = name.downcase.gsub(" ", "_") + "-icon.png"
  end
end
