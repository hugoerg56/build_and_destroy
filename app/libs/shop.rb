class Shop
  attr_accessor :trinkets
  TRINKETS = {"Food" => 50, "Diamond Ring" => 1000 , "Champagne Bottle" => 500, "Box of Cigars" =>  250, "Cigarettes" =>  250, "Gold Chain" => 2500, "Shoes" =>  300, "Purse" =>  250, "Roses" =>  100, "Food" =>  50, "Dessert" =>  50, "Sneakers" =>  300, "TNT" =>  100, "Bulldozer" =>  500}
  # "Lipstick" =>  100, "Perfume" =>  200,
  def initialize
    define_trinkets
    #define_achievements
  end

  def define_trinkets
    self.trinkets = []
    TRINKETS.each do |name, price|
      self.trinkets << Trinket.new(name, price)
    end
  end
end

