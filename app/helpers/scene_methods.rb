module SceneMethods
  
  def check_menu(touch)
    @buttons.each do |c|
      TouchHelper.open_on_match_touch(c, touch) 
    end  
  end
  
  def check_menu2(touch)
    @buttons2.each do |c|
      TouchHelper.open_on_match_touch(c, touch) 
    end  
  end  
  
  def check_menu_new_action(touch)
    @buttons_new_action.each do |c|
      TouchHelper.open_on_match_touch(c, touch, false, true) 
    end  
  end  
  
  def check_menu_new_action3(touch)
    @buttons_new_action3.each do |c|
      TouchHelper.open_on_match_touch(c, touch, false, true) 
    end  
  end    
  
  def check_menu_new_action2(touch)
    @buttons_new_action2.each do |c|
      TouchHelper.open_on_match_touch(c, touch, false, true) 
    end  
  end  
  
  def check_menu_layout(touch)
    @buttonsl.each do |c|
      TouchHelper.open_on_match_touch_layout(c, touch) 
    end  
  end  
  
  def check_cards_touch(touch)
    @bcards.each do |c|
      if GameHelper.match_cards_touch(c, touch) 
        return c
      end  
    end  
    return nil
  end  
  
  def check_cards_touch_board_pos(touch)
    $board_positions.each do |p|
      if GameHelper.card_match_pos(p, touch) 
        return p
      end  
    end  
    return false
  end  
  
  def check_menu_action(touch)
    $amenu.each do |m|
      if GameHelper.card_match_pos(m[0], touch) 
        self.send(m[1])
        return m
      end   
    end  
  end  
  
  def add_line_vs
    line1 = MG::Draw.new
    line1.line([0, ImageHelper.top_porcent(48.5)], [ImageHelper.left_porcent(42), ImageHelper.top_porcent(48.5)], 2.0, [1.0, 1.0, 1.0, 1.0])
    
    line2 = MG::Draw.new
    line2.line([ImageHelper.left_porcent(58), ImageHelper.top_porcent(48.5)], [ImageHelper.left_porcent(100), ImageHelper.top_porcent(48.5)], 2.0, [1.0, 1.0, 1.0, 1.0])
        
    
    vs = MG::Text.new('Vs', 'OpenSans-Bold', 92)
    vs.position = [$visible_size.width / 2, ImageHelper.top_porcent(48)]
    vs.text_color = [1.0,1.0,1.0,1.0]
    
    add line1
    add line2    
    add vs
  end  
  
  def clean_and_start
    $main_view_objs.each{|o| o.removeFromSuperview}
    $director.replace(GameScene.new)
  end  
end  