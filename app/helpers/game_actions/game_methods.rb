module GameMethods
  
  def init_game_vars
    @bcards = []
    @action_cards = []
    $amenu = []    
    @action_menu_items = []
    $l_player_cards = []
    $l_player_cards_objs = []
    $o_player_cards = []
    $o_player_cards_objs = []
    $valid_actions = []
    @deal_elements = []  
    @hightlight_items = []
    $my_turn_actions = ""
    $round = 1
    $rr = 0
    $hl_positions = []
    $hl_positions2 = []
    $ohl_positions2 = []
    @start_player = $game.players.first 
    $next_player = $game.players.first 
    $turn = false
    $showing_message = false
    $positions_cards = 0.upto(15).map{|a| []}
    $o_player_back_cards = []
    
    if @start_player == $game.local_player
      $turn = true
    end  
       
    $board_positions = []  
    
  end  
  
  def next_player
    $next_player = $game.players.first == $next_player ? $game.players.last : $game.players.first
  end  
  
  def show_actions(object, position, view)    
    $object = object
    $position_i = $board_positions.find_index(position)
    
    center_card_on_position($object[0], position)    
    actions = ['cancel']
    
    $game.available_actions($object[2], $game.local_player, $position_i).each{|a| actions << a.to_s}
    if actions != ["cancel", "place_card"]
      display_actions(actions, object, position, view)
    else
      if $positions_cards[$position_i].count == 0
        place_card 
      else
        $valid_actions = actions
        if $valid_actions == ["cancel", "pick_up_build", "place_card"]
          actions = ['cancel', "place_card"]
        else
          actions = ['cancel']
        end
        show_new_action_menu unless @menu_visible  
        actions.each_with_index{|a, i| add_action(a, i+1)}
      end  
    end    
  end  
  
  def display_actions(actions, object, position, view)
    $amenu = []
    $valid_actions = actions
    action_shadow = MG::Sprite.new(ImageHelper.name_for_phone("action_shadow.png"))
    action_shadow.position = [(action_shadow.size.width / 2), (action_shadow.size.height / 2)]
    @action_menu_items << action_shadow
    
    view.add action_shadow, 5
    
    if $valid_actions == ["cancel", "pick_up_build", "place_card"]
      actions = ['cancel', "place_card"]
    else
      actions = ['cancel']
    end
    show_new_action_menu unless @menu_visible
    actions.each_with_index{|a, i| add_action(a, i+1)}
  end 
  
  
  def add_action(name, i)
    l = MG::Layout.new
    l.size = [($visible_size.width / 4), ImageHelper.porcent_for($visible_size.height, 24)]
    l.position = [($visible_size.width / 4) * (4 - i), ImageHelper.porcent_for($visible_size.height, 0)]
    
    icon = MG::Sprite.new(ImageHelper.name_for_phone("#{name}.png"))
    icon.position = [ l.size.width / 2, l.size.height / 2]
    l.add icon
    
    icont = MG::Text.new(title_for_action(name), 'OpenSans-Bold', 29)
    icont.area_size = [l.size.width, 30]
    icont.position = [l.size.width / 2, icon.position.y - icon.size.height + 50]
    icont.horizontal_align = :center
    icont.text_color = [1.0,1.0,1.0,1.0]
    l.add icont
    
    add l, 6
    $amenu << [l, name]
    @action_menu_items << l
  end
  
  def title_for_action(action)
    action.gsub("_", " ").split.map(&:capitalize).join(' ')
  end  
  
  def clean_action_menu_move
    @action_menu_items.each{|i| i.delete_from_parent}
    @action_menu_items = []
  end  
  
  def clean_action_menu
    @action_menu_items.each{|i| i.delete_from_parent}
    @action_menu_items = []
    $valid_actions = []
    $l_player_cards.each do |c| 
      c.move_to(original_position_of(c), 0.5) 
    end  
  end 
  
  def simple_clean_action_menu
    @action_menu_items.each{|i| i.delete_from_parent}
    @action_menu_items = []
    $valid_actions = []
  end  
  
  def restore_cards_scale(card)
    $l_player_cards.each{|c| c.scale = scale_of(card)}
  end  
  
  def scale_of(card)
    ImageHelper.porcent_for($visible_size.width, 15) / card.size.width
  end 
  
  def center_card_on_position(mcard, layout, time=0.4, position_i=nil)
    
    posit = [layout.position.x + (layout.size.width / 2), layout.position.y + (layout.size.height / 2)]
    mcard.move_to(posit, time)
    
    if position_i
      EM.add_timer (time + 0.03) do
        add_card_to_pos(mcard, position_i) 
        0.upto(15).each{|n| set_card_rotate_for_position(n) }
      end  
    end  
    
    
  end  
  
  def next_player_turn
    $turn = false
    #restart_timer if $versus == "user"
    clean_all_hightlights
    @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
    @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)    
    @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 68.83), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
    @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)    
      
    if $game.opponent_player.hand.size != 0
      computer_move    
    else
      NSLog("Opponent: #{$game.opponent_player.hand.size}")
      NSLog("Local: #{$game.local_player.hand.size}")
      NSLog("Deck: #{$game.deck.cards.count}")
      if $game.deck.cards.count == 0 and $game.opponent_player.hand.size == 0 and $game.local_player.hand.size == 0
        mp "END OF DECK3"  
        new_deck 
      else  
        new_round if $versus == "compu"
        if $game.deck.cards.count == 0
          show_flashing_message("cards")
          EM.add_timer 1.4 do
            hide_message
          end  
        end
        EM.add_timer 1.5 do
          computer_move 
        end
      end  
    end  
    0.upto(15).each{|n| set_card_rotate_for_position(n) }
    #update_scores
  end  
  
  def show_message(name, number=nil)
    if name == "stacking" or name == "ostacking"
      $showing_message = true
      $message_image = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      
      label = UILabel.alloc.initWithFrame(CGRectMake(0, 80, @screen_width, 50))
      label.setText(number_name(number).upcase)
      label.setTextColor(UIColor.whiteColor)
      label.setTextAlignment(NSTextAlignmentCenter)
      label.setFont(UIFont.fontWithName("OpenSans-Bold", size:25.0))
      
      $message_image2 = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      $message_image2.image = UIImage.imageNamed(ImageHelper.name_for_phone("shadow.png"))
      $message_image.addSubview($message_image2)
      $message_image.addSubview(label)
      $main_view.addSubview($message_image)
      SoundHelper.got_item   
    elsif name == "stack" or name == "ostack"
      $showing_message = true
      $message_image = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      
      label = UILabel.alloc.initWithFrame(CGRectMake(0, 80, @screen_width, 50))
      label.setText(stack_name(number).upcase)
      label.setTextColor(UIColor.whiteColor)
      label.setTextAlignment(NSTextAlignmentCenter)
      label.setFont(UIFont.fontWithName("OpenSans-Bold", size:25.0))
      
      $message_image2 = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      $message_image2.image = UIImage.imageNamed(ImageHelper.name_for_phone("shadow.png"))
      $message_image.addSubview($message_image2)
      $message_image.addSubview(label)
      $main_view.addSubview($message_image)
      SoundHelper.got_item       
      
    elsif name == "invalid" or name == "cards"
      $showing_message = true
      $message_image = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      
      label = UILabel.alloc.initWithFrame(CGRectMake(0, 80, @screen_width, 50))
      label.setText(name == "invalid" ? "Invalid Play" : "CARDS")
      label.setTextColor(UIColor.whiteColor)
      label.setTextAlignment(NSTextAlignmentCenter)
      label.setFont(UIFont.fontWithName("OpenSans-Bold", size:25.0))
      
      $message_image2 = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      $message_image2.image = UIImage.imageNamed(ImageHelper.name_for_phone("shadow.png"))
      $message_image.addSubview($message_image2)
      $message_image.addSubview(label)
      $main_view.addSubview($message_image)
      SoundHelper.got_item       
    else  
      $showing_message = true
      $message_image = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      $message_image.image = UIImage.imageNamed(ImageHelper.name_for_phone("#{name}_message.png"))
      $main_view.addSubview($message_image)
      SoundHelper.got_item 
    end  
  end  
  
  def stack_name(name)
    case name.to_s
    when "K"
      "Stack Kinkgs"
    when "Q"
      "Stack Queens"
    when "J"
      "Stack Jacks"   
    when "A"
      "Stack Aces"         
    else
      "Stack #{name}'s"
    end  
  end  
  
  def number_name(name)
    case name.to_s
    when "K"
      "Stacking Kinkgs"
    when "Q"
      "Stacking Queens"
    when "J"
      "Stacking Jacks"   
    when "A"
      "Stacking Aces"         
    else
      "Stacking #{name}'s"
    end       
  end  
  
  def show_flashing_message(name, number=nil)
    $showing_message = true
    
    $message_image2 = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
    $message_image2.image = UIImage.imageNamed(ImageHelper.name_for_phone("shadow.png"))    
    $main_view.addSubview($message_image2)    
    
    if name == "cards"
      $message_image_view = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
    
      label = UILabel.alloc.initWithFrame(CGRectMake(0, ((@screen_height / 2) - (@screen_height * 0.1)), @screen_width, 50))
      label.setText(name == "invalid" ? "Invalid Play" : "CARDS")
      label.setTextColor(UIColor.whiteColor)
      label.setTextAlignment(NSTextAlignmentCenter)
      label.setFont(UIFont.fontWithName("OpenSans-Bold", size:25.0))
    
      $message_image_view.addSubview($message_image2)
      $message_image_view.addSubview(label)
      $main_view.addSubview($message_image_view)
    elsif name == "stack" or name == "ostack"
      $message_image_view = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
  
      label = UILabel.alloc.initWithFrame(CGRectMake(0, ((@screen_height / 2) - (@screen_height * 0.1)), @screen_width, 50))
      label.setText(stack_name(number).upcase)
      label.setTextColor(UIColor.whiteColor)
      label.setTextAlignment(NSTextAlignmentCenter)
      label.setFont(UIFont.fontWithName("OpenSans-Bold", size:25.0))
  
      $message_image_view.addSubview($message_image2)
      $message_image_view.addSubview(label)
      $main_view.addSubview($message_image_view)        
      
    elsif name == "stacking" or name == "ostacking"
      $message_image_view = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
  
      label = UILabel.alloc.initWithFrame(CGRectMake(0, ((@screen_height / 2) - (@screen_height * 0.1)), @screen_width, 50))
      label.setText(number_name(number).upcase)
      label.setTextColor(UIColor.whiteColor)
      label.setTextAlignment(NSTextAlignmentCenter)
      label.setFont(UIFont.fontWithName("OpenSans-Bold", size:25.0))
  
      $message_image_view.addSubview($message_image2)
      $message_image_view.addSubview(label)
      $main_view.addSubview($message_image_view)    
    else

      $message_image_view = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      $message_image = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      $message_image.image = UIImage.imageNamed(ImageHelper.name_for_phone("#{name}_message2.png"))    
      $message_image_view.addSubview($message_image)    
      $main_view.addSubview($message_image_view)
    
    end
    
    default_val = 1
    $mtimer = EM.add_periodic_timer 0.3 do
      default_val = default_val == 1 ? 0 : 1
      $message_image_view.animate(duration: 0.1){ $message_image_view.alpha = default_val}
    end
    
    SoundHelper.got_item    
  end  
  
  def hide_message
    EM.cancel_timer($mtimer) if $mtimer
    $showing_message = false
    $message_image.removeFromSuperview if $message_image
    $message_image_view.removeFromSuperview if $message_image_view
    $message_image2.removeFromSuperview if $message_image2
    $message_image2 = nil
    $message_image = nil
    $message_image_view = nil
    $mtimer = nil
  end  
  
  def cancel 
    restore_cards_scale($object[0])
    clean_action_menu
    $amenu = [] 
    @hightlight_items.each do |i|
      i.delete_from_parent
    end
    @hightlight_items = []
    $hl_positions = []
  end    
  
  def place_card_group(response, hl_pos)
    mp "TODO check next step"
    hl_posi = hl_pos.to_s.gsub('[', '').gsub(']', '').gsub(' ', '')
        
    mp "TODO: show selection screen"
    show_card_selection(response) if response[0] and response[1]  
    
  end  
  
  def card_not_included?(c, ar)
    ar.select{|r| r.rank == c.rank}.count <= 0
  end  
  
  def show_card_selection(response)
    
    card1 = response[0]
    card2 = response[1]    
    
    if card1 != nil and card2 != nil
    
      response_aux = []
      response.each do |c|
        response_aux << c if card_not_included?(c, response_aux)
      end  
      response = response_aux
      mp "CARD SELECTION >>>>>"
      mp response
      mp "CARD SELECTION <<<<<"    
      $selection_response = response
      #Add Shadow
      $message_image_view_back = UIView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      $message_image_back = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, @screen_width, @screen_height))
      $message_image_back.image = UIImage.imageNamed(ImageHelper.name_for_phone("all_shadow.png"))        
  
      label = UILabel.alloc.initWithFrame(CGRectMake(0, 50, @screen_width, 50))
      label.setText("What are you trying to build?")
      label.setTextColor(UIColor.whiteColor)
      label.setTextAlignment(NSTextAlignmentCenter)
      label.setFont(UIFont.fontWithName("OpenSans-Bold", size:22.0))
    
      $message_image_view_back.addSubview($message_image_back)
      $message_image_view_back.addSubview(label)
       
      #display_actions(["cancel"], nil, nil, self)
    
      #Display Cards Options
    
      card_width = ($main_view.frame.size.width * 0.3328)
      card_height = (card_width * 1.458333)
    
      @card1img = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, card_width, card_height))
      @card1img.image = UIImage.imageNamed("#{card1.id}-2208-reg.png") 
      @card1img.position = [(($main_view.frame.size.width / 4)), (($main_view.frame.size.height / 2) - (card_height / 2))]
    
      @card2img = UIImageView.alloc.initWithFrame(CGRectMake(0, 0, card_width, card_height))
      @card2img.image = UIImage.imageNamed("#{card2.id}-2208-reg.png") 
      @card2img.position = [(($main_view.frame.size.width / 4) * 3), (($main_view.frame.size.height / 2) - (card_height / 2))]
    
      @buttons_new_action3 << [@card1img, "group_card1-Call", self]   
      @buttons_new_action3 << [@card2img, "group_card2-Call", self]   
      
      $message_image_view_back.addSubview(@card1img)    
      $message_image_view_back.addSubview(@card2img)
    
      $main_view.addSubview($message_image_view_back) 
      $grupo_visible = true
      
    end
      
  end
  
  def group_card1
    mp "grupoe 1"
    group_card($selection_response.first)
  end
  
  def group_card2
    mp "grupoe 2"
    group_card($selection_response.last)
  end    
  
  def move_board_card_positions(origin, destination)
    cards_in_origin = $game.board_cards.select{|c| c.position == origin}
    cards_in_origin.each do |board_card| 
      mp "UPDATING POSITION FOR CARD #{board_card.card.rank} #{board_card.card.suit}"
      $game.update_board_card(board_card, $game.local_player, destination) 
    end  
  end  
  
  def place_card
    mp "User place card #{$object[2].rank} #{$object[2].suit} on position #{$position_i}"
    hl_pos = $hl_positions != [] ? $hl_positions : nil
    response = $game.place_card($object[2], $game.local_player, $position_i, hl_pos)
    mp "Positions"
    mp hl_pos
    mp " >>> Place Card RESPONSE"
    mp response
    mp " <<< Place Card RESPONSE"    
    
    unless response.kind_of?(Array)
      unless response == 'Invalid play'
        add_card_to_pos($object[0], $position_i)
        action = response == "Stacking Number" ? "stacking_number" : "place_card" 
        
        $my_turn_actions = "opponent_action:::#{action}::#{$object[2].id}::#{$position_i}::"
   
        $l_player_cards_objs.delete($object[2])
        $l_player_cards.delete($object[0])
        @to_delete = [$object[0], 'select_card', $object[2]]
        @bcards.delete([$object[0], 'select_card', $object[2]])

        clean_action_menu
        $amenu = []
    
        next_player_turn
      else
        cancel  
      end  
    else  
      place_card_group(response, hl_pos)
    end  
  end     
  
  def oplace_card(card_id, position, hl_pos=nil)
    
    if hl_pos
      hl_pos = hl_pos.split(",") 
      if hl_pos.kind_of?(Array)
        hl_pos = hl_pos.map{|n| n.to_i}
      end  
    end  
    
    card = $o_player_cards_objs.select{|c| c.id == card_id}.last
    card_index = $o_player_cards_objs.find_index(card)
    card_visual = $o_player_cards[card_index]

    response = $game.place_card(card, $game.opponent_player, position, hl_pos)

    if response.kind_of?(Array)
      oplace_card_group(response, card, card_visual, position,  hl_pos)
    else  
      move_compu_card(card_visual, $board_positions[position], position, card_id)
      EM.add_timer 1.2 do
        @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)

        @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
        @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
        $turn = true
        restart_timer
      end  
    end
  end  
  

  def group_card(card)
    
    hl = $hl_positions.to_s.gsub('[', '').gsub(']', '').gsub(' ', '')     
    
    
    $message_image_view_back.removeFromSuperview if $message_image_view_back
    
    agrupar = $game.group_builds_in_positions(card, $hl_positions)  
    
    $my_turn_actions = "opponent_action:::place_card_group::#{card.id}::#{$position_i}::#{hl == "" ? " " : hl}::#{$object[2].id}:: ::#{agrupar.to_s}"
    
    if agrupar
      add_card_to_pos($object[0], $position_i)
      
      $l_player_cards_objs.delete($object[2])
      $l_player_cards.delete($object[0])
      @to_delete = [$object[0], 'select_card', $object[2]]
      @bcards.delete([$object[0], 'select_card', $object[2]])
      
      agrupar.each do |grupo|
        positions_to_move = grupo.flatten
        $hl_positions2 << grupo.first
        if positions_to_move.count > 1
          position_destination = grupo.first
          positions_to_move.each do |p|
            if p != position_destination
              move_board_card_positions(p, position_destination)
              $positions_cards[p].each do |c|     
                move_compu_card(c, $board_positions[position_destination], position_destination, nil)
              end
              $positions_cards[p] = [] 
            end  
          end  
        end  
      end  
      
    end  
    
    clean_action_menu
    $amenu = []

    next_player_turn
    
    @hightlight_items.each do |i|
      i.delete_from_parent
    end
    $grupo_visible = false
    @buttons_new_action3 = []
    @hightlight_items = []
    $hl_positions = []
    
  end  
  
  

  def oplace_card_group(card_action, position, hl_pos, card, extra, agrupar)

    card_action = $o_player_cards_objs.select{|c| c.id == card_action}.last
    card = $o_player_cards_objs.select{|c| c.id == card.to_i}.last
    card_visual = $o_player_cards[$o_player_cards_objs.find_index(card)]
    hl_pos = hl_pos.split(",").map(&:to_i)

    add_card_to_pos(card_visual, position)
    $game.board_cards.push(BoardCard.new(card, $game.opponent_player, position, 2))
    $o_player_cards_objs.delete(card)
    $o_player_cards.delete(card_visual)
    
    #agrupar = $game.group_builds_in_positions(card_action, hl_pos)  

    agrupar = [agrupar.to_s.gsub('[', '').gsub(']', '').gsub(' ', '').split(",").map(&:to_i)]
    
    if agrupar
      agrupar.each do |grupo|
        positions_to_move = grupo.flatten
        $hl_positions2 << grupo.first
        if positions_to_move.count > 1
          position_destination = grupo.first
          positions_to_move.each do |p|
            if p != position_destination
              move_board_card_positions(p, position_destination)
              $positions_cards[p].each do |c|     
                move_compu_card(c, $board_positions[position_destination], position_destination, nil)
              end
              $positions_cards[p] = [] 
            end  
          end  
        end  
      end  

    end  
   
    EM.add_timer 1.2 do
      @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)

      @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
      @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      $turn = true
      restart_timer
    end 
    
  end   
  
  
  def ostacking_number(card_id, position)
    card = $o_player_cards_objs.select{|c| c.id == card_id}.last    
    show_flashing_message("ostacking", card.rank)
    oplace_card(card_id, position)
    EM.add_timer 1.6 do
      hide_message
    end 
  end  
  
  def opick_up(card_id, position)
    card = $o_player_cards_objs.select{|c| c.id == card_id}.last
    card_index = $o_player_cards_objs.find_index(card)
    card_visual = $o_player_cards[card_index]
    mp "Opponent pick_up card #{card.rank} #{card.suit} on position #{position}"
    $game.pick_up(card, $game.opponent_player, position)

    move_compu_card(card_visual, $board_positions[position], position, card_id)
    
    $o_player_cards_objs.delete(card)
    $o_player_cards.delete(card_visual)
    
    #update_scores
    EM.add_timer 1.2 do
      clear_position(position)
      @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)

      @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
      @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      $turn = true
      restart_timer
    end  
  end    
  
  def pick_up
    mp "User pickup cards using card #{$object[2].rank} #{$object[2].suit} on position #{$position_i}"
    $game.pick_up($object[2], $game.local_player, $position_i)
    add_card_to_pos($object[0], $position_i)
    
    $my_turn_actions = "opponent_action:::pick_up::#{$object[2].id}::#{$position_i}"
    
    $l_player_cards_objs.delete($object[2])
    $l_player_cards.delete($object[0])
    @bcards.delete([$object[0], 'select_card', $object[2]])
    
    clear_position($position_i)
    clean_action_menu
    $amenu = []
    next_player_turn
  end
  
  def start_build
    mp "User start new build using card #{$object[2].rank} #{$object[2].suit} on position #{$position_i}"
    $game.start_build($object[2], $game.local_player, $position_i)
    add_card_to_pos($object[0], $position_i)
    
    $my_turn_actions = "opponent_action:::start_build::#{$object[2].id}::#{$position_i}"
    
    $l_player_cards_objs.delete($object[2])
    $l_player_cards.delete($object[0])
    @bcards.delete([$object[0], 'select_card', $object[2]])

    clean_action_menu
    $amenu = []
    next_player_turn    
  end
  
  def ostart_build(card_id, position)
    card = $o_player_cards_objs.select{|c| c.id == card_id}.last
    card_index = $o_player_cards_objs.find_index(card)
    card_visual = $o_player_cards[card_index]    
    
    mp "Opponent start new build using card #{card.rank} #{card.suit} on position #{position}"
    $game.start_build(card, $game.opponent_player, position)
    
    move_compu_card(card_visual, $board_positions[position], position, card_id)
    $o_player_cards_objs.delete(card)
    $o_player_cards.delete(card_visual)
    #update_scores
    EM.add_timer 1.2 do
      @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)

      @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
      @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      $turn = true
      restart_timer
    end   
  end  
  
  def ocreate_build(card_id, position, hl_positions2=[], hl_positions=[], card_active=nil)
    
    if card_active and card_active != " " and card_active != 0
      card = $o_player_cards_objs.select{|c| c.id == card_active}.last
      card_index = $o_player_cards_objs.find_index(card)
      card_visual = $o_player_cards[card_index]
    else
      card = $o_player_cards_objs.select{|c| c.id == card_id}.last
      card_index = $o_player_cards_objs.find_index(card)
      card_visual = $o_player_cards[card_index] 
    end  

    hl_positions = [] if hl_positions == " " or hl_positions == ""
    hl_positions = hl_positions.split(",") if hl_positions != []
    
    if hl_positions.kind_of?(Array)
      hl_positions = hl_positions.map{|n| n.to_i}
    end  

    if hl_positions2.kind_of?(Array)
      hl_positions2 = hl_positions2.map{|n| n.to_i}
    end      
    
    mp "Opponent create new build using card #{card.rank} #{card.suit} on position #{position}"
    
    if $ohl_positions2 != [] 
      response =  $game.move_to_pick_up_build($ohl_positions2 , card, $game.opponent_player)
      $ohl_positions2 = []

      if response[0].kind_of?(Array)
        response[0].each{|f| clear_position(f)}
        new_message = response[2].gsub(" ", "_").downcase
      end  
    else   

      if hl_positions != [] and hl_positions.count > 1
        @flag1 = true
        response =  $game.simple_move_to_pick_up_build(hl_positions, card, $game.opponent_player)
        message = response.kind_of?(Array) ? response.last : response
        oanimate_new_build(response, message, card)
        index = card.id
        $o_player_back_cards.select{|a| a[1].to_i == index }.last[0].delete_from_parent if index
      else
        $game.pick_up_build(card, $game.opponent_player, position)
      end
    end
    
    if response != "Invalid play" and @flag1 != true
      add_card_to_pos(card_visual, position)
    
      clear_position(position)
      $l_player_cards_objs.delete(card)
      $l_player_cards.delete(card_visual)
      @bcards.delete([card_visual, 'select_card', card])
    
      show_message(new_message ? new_message : "obuild")
    
      EM.add_timer 1.8 do
        hide_message
        @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)

        @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
        @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
        $turn = true
        restart_timer
      end
    else  
      mp "PARECE INVALID"
      if @flag1 != true
        show_message("invalid")    
        EM.add_timer 1.8 do
          hide_message
          @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)

          @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
          @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
          $turn = true
          restart_timer
        end  
      end  
    end
    @flag1 = false
 
  end    
  
  def create_build
    mp "User create build using card #{$object[2].rank} #{$object[2].suit} on position #{$position_i}"
    
    if $hl_positions2 != [] 
      hl2 = $hl_positions2.to_s.gsub('[', '').gsub(']', '').gsub(' ', '')
      hl = $hl_positions.to_s.gsub('[', '').gsub(']', '').gsub(' ', '')
          
      $my_turn_actions = "opponent_action:::create_build::#{$object[2].id}::#{$position_i}::#{hl2 == "" ? " " : hl2}:: :: "        
      
      
      response =  $game.move_to_pick_up_build($hl_positions2, $object[2], $game.local_player)
      if response[0].kind_of?(Array)
        response[0].each{|f| clear_position(f)}
        new_message = response[2].gsub(" ", "_").downcase
      end  
      $my_turn_actions = "opponent_action:::create_build::#{$object[2].id}::#{$position_i}::#{$hl_positions2}"
    
      $hl_positions2 = []
    else   
      if $hl_positions != [] and $hl_positions.count > 1
        @flag1 = true
        
        hl2 = $hl_positions2.to_s.gsub('[', '').gsub(']', '').gsub(' ', '')
        hl = $hl_positions.to_s.gsub('[', '').gsub(']', '').gsub(' ', '')
            
        $my_turn_actions = "opponent_action:::create_build::#{$object[2].id}::#{$position_i}::#{hl2 == "" ? " " : hl2}::#{hl == "" ? " " : hl}::#{@active[2].id}"        
        response =  $game.simple_move_to_pick_up_build($hl_positions, @active[2], $game.local_player)
        message = response.kind_of?(Array) ? response.last : response
        animate_new_build(response, message)
        
      else
        response = $game.pick_up_build($object[2], $game.local_player, $position_i)

      end    
    end

    if response != "Invalid play" and @flag1 != true
      add_card_to_pos($object[0], $position_i)
    
      $my_turn_actions = "opponent_action:::create_build::#{$object[2].id}::#{$position_i}"
      
      clear_position($position_i)
      $l_player_cards_objs.delete($object[2])
      $l_player_cards.delete($object[0])
      @bcards.delete([$object[0], 'select_card', $object[2]])
    
      show_message(new_message ? new_message : "build")
    
      EM.add_timer 1.8 do
        clean_action_menu
        $amenu = []
        hide_message
        EM.add_timer 1.0 do
          next_player_turn
        end    
      end
    else  
      $amenu = []
      if @flag1 != true
        show_message("invalid")    
        EM.add_timer 1.8 do
          hide_message
        end  
      end  
    end
    @flag1 = false
  end    
  
  
  def animate_new_build(response, message)
    mp "animate_new_build"
    mp response
    mp message
    if response.kind_of?(Array) and message != "Invalid play"
      
      positions_to_move = response.flatten.select{|g| g.kind_of?(Integer)}
      position_destination = positions_to_move.first
      
      positions_to_move.shift 
      positions_to_move.each do |p|
        $positions_cards[p].each do |c|   
          move_compu_card(c, $board_positions[position_destination], position_destination, nil)
        end
        clear_position(p) if p != position_destination
        $positions_cards[p] = []
      end  
      index = $l_player_cards_objs.find_index(@card_t[2])
      
      @hightlight_items.each do |i|
        i.delete_from_parent
      end
      @hightlight_items = []
      $hl_positions = []
      
      if message == nil
        show_message("build")
      else
        show_message(message.downcase.gsub(" ", "_"))
      end    
      
      
      center_card_on_position($l_player_cards[index], $board_positions[position_destination], 0.8, position_destination) 
      
      EM.add_timer 1.8 do
        clear_position(position_destination)
        
        $l_player_cards_objs.delete(@card_t[2])
        $l_player_cards.delete($l_player_cards[index])
        @bcards.delete([@card_t[0], 'select_card', @card_t[2]])
        
        simple_clean_action_menu
        hide_message
        EM.add_timer 1.0 do
          next_player_turn
        end    
      end  
    else
      @hightlight_items.each do |i|
        i.delete_from_parent
      end
      @hightlight_items = []
      $hl_positions = []     
      
      show_message("invalid")    
      EM.add_timer 1.8 do
        hide_message
      end   
    end
  
  end
  
  
  def oanimate_new_build(response, message, card=nil)
    mp "animate_new_build"
    mp response
    mp message
    if response.kind_of?(Array) and message != "Invalid play"
      
      positions_to_move = response.flatten.select{|g| g.kind_of?(Integer)}
      position_destination = positions_to_move.first
      
      positions_to_move.shift 
      positions_to_move.each do |p|
        $positions_cards[p].each do |c|   
          move_compu_card(c, $board_positions[position_destination], position_destination, nil)
        end
        clear_position(p) if p != position_destination
        $positions_cards[p] = []
      end  
      index = $o_player_cards_objs.find_index(card)

      if message == nil
         show_message("obuild")
      else
         show_message("o#{message.downcase.gsub(" ", "_")}")
      end    
     
      center_card_on_position($o_player_cards[index], $board_positions[position_destination], 0.8, position_destination) 
      
      EM.add_timer 1.8 do
        clear_position(position_destination)
        
        $o_player_cards_objs.delete(card)
        $o_player_cards.delete($o_player_cards[index])
        
        hide_message
        @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)

        @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
        @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
        $turn = true  
        restart_timer
      end  
    else   
      show_message("invalid")    
      EM.add_timer 1.8 do
        hide_message
        @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)

        @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
        @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
        $turn = true
        restart_timer
      end   
    end
  
  end  
  
  def pick_up_build
    create_build
  end  
  
  def opick_up_build(card_id, position)
    mp "HL2-2"*20
    mp $ohl_positions2
    ocreate_build(card_id, position, $ohl_positions2)
  end  
  
  def create_double_build
    @hightlight_items.each do |i|
      i.delete_from_parent
    end
    @hightlight_items = []

    index = $l_player_cards_objs.find_index($high_card)
    @bcards.delete([$l_player_cards[index], 'select_card', $l_player_cards_objs[index]])
    
    $my_turn_actions = "opponent_action:::create_double_build::#{$l_player_cards_objs[index].id}::#{0}"    
    
    $temp_position = get_first_empty_position
    $game.move_cards_to_position($high_card, $game.local_player, $temp_position)
    $game.pick_up_build($high_card, $game.local_player, $temp_position) 
    
    $game.local_player.hand.delete($l_player_cards_objs[index])    
    $l_player_cards_objs.delete($l_player_cards_objs[index])
    $l_player_cards[index].delete_from_parent
    $l_player_cards.delete($l_player_cards[index])    
    
    $hl_positions.each{|p| clear_position(p)}
    $hl_positions = []
    
    show_message("double_build")
    
    EM.add_timer 1.8 do
      clean_action_menu
      $amenu = []
      hide_message
      EM.add_timer 1.0 do
        next_player_turn
      end    
    end
  end  
  
  def ocreate_double_build(card_id, position)
    card = $o_player_cards_objs.select{|c| c.id == card_id}.last
    card_index = $o_player_cards_objs.find_index(card)
    card_visual = $o_player_cards[card_index]   
    
    $h_position = []
    
    available_builds = $game.available_builds?(card)
    available_builds.each do |p|
      if p.is_a?(Array)
        p.each{|i| $h_position << i}
      else
        $h_position < i
      end    
    end  
    
    $game.opponent_player.hand.delete($o_player_cards_objs[card_index])    
    $o_player_cards_objs.delete($o_player_cards_objs[card_index])
    $o_player_cards[card_index].delete_from_parent
    $o_player_cards.delete($o_player_cards[card_index])

    $otemp_position = get_first_empty_position
    $game.move_cards_to_position(card, $game.opponent_player, $otemp_position)
    $game.pick_up_build(card, $game.opponent_player, $otemp_position) 

    $h_position.each{|p| clear_position(p)}
    $h_position = []
    #update_scores
    
    show_message("double_build")
    EM.add_timer 1.8 do
      clear_position(position)
      @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)
      hide_message
      @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
      @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      $turn = true  
      restart_timer
    end
  end    
  
  def create_triple_build
    @hightlight_items.each do |i|
      i.delete_from_parent
    end
    @hightlight_items = []

    index = $l_player_cards_objs.find_index($high_card)
    @bcards.delete([$l_player_cards[index], 'select_card', $l_player_cards_objs[index]]) 

    $my_turn_actions = "opponent_action:::create_triple_build::#{$l_player_cards_objs[index].id}::#{0}"

    $game.local_player.hand.delete($l_player_cards_objs[index])
    $l_player_cards_objs.delete($l_player_cards_objs[index])
    $l_player_cards[index].delete_from_parent
    $l_player_cards.delete($l_player_cards[index])

    $temp_position = get_first_empty_position
    $game.move_cards_to_position($high_card, $game.local_player, $temp_position)
    $game.pick_up_build($high_card, $game.local_player, $temp_position) 
    
    $hl_positions.each{|p| clear_position(p)}
    $hl_positions = []
    
    
    show_message("triple_build")
    
    EM.add_timer 1.8 do
      clean_action_menu
      $amenu = []
      hide_message
      EM.add_timer 1.0 do
        next_player_turn
      end    
    end
  end    
  
  def ocreate_triple_build(card_id, position)
    card = $o_player_cards_objs.select{|c| c.id == card_id}.last
    card_index = $o_player_cards_objs.find_index(card)
    card_visual = $o_player_cards[card_index]   
    
    $h_position = []
    
    available_builds = $game.available_builds?(card)
    available_builds.each do |p|
      if p.is_a?(Array)
        p.each{|i| $h_position << i}
      else
        $h_position < i
      end    
    end  
    
    $game.opponent_player.hand.delete($o_player_cards_objs[card_index])    
    $o_player_cards_objs.delete($o_player_cards_objs[card_index])
    $o_player_cards[card_index].delete_from_parent
    $o_player_cards.delete($o_player_cards[card_index])

    $otemp_position = get_first_empty_position
    $game.move_cards_to_position(card, $game.opponent_player, $otemp_position)
    $game.pick_up_build(card, $game.opponent_player, $otemp_position) 

    $h_position.each{|p| clear_position(p)}
    $h_position = []
    show_message("triple_build")
    
    #update_scores
    EM.add_timer 1.8 do
      clear_position(position)
      @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)
      hide_message
      @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
      @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      $turn = true
      restart_timer  
    end
  end    
    
  
  def create_quadruple_build
    @hightlight_items.each do |i|
      i.delete_from_parent
    end
    @hightlight_items = []

    index = $l_player_cards_objs.find_index($high_card)
    
    @bcards.delete([$l_player_cards[index], 'select_card', $l_player_cards_objs[index]])
    $my_turn_actions = "opponent_action:::create_quadruple_build::#{$l_player_cards_objs[index].id}::#{0}"
    
    $game.local_player.hand.delete($l_player_cards_objs[index])
    $l_player_cards_objs.delete($l_player_cards_objs[index])
    $l_player_cards[index].delete_from_parent
    $l_player_cards.delete($l_player_cards[index])

    $temp_position = get_first_empty_position
    $game.move_cards_to_position($high_card, $game.local_player, $temp_position)
    $game.pick_up_build($high_card, $game.local_player, $temp_position) 
    
    $hl_positions.each{|p| clear_position(p)}
    $hl_positions = []
    
    
    show_message("quadruple_build")
    
    EM.add_timer 1.8 do
      clean_action_menu
      $amenu = []
      hide_message
      EM.add_timer 1.0 do
        next_player_turn
      end    
    end
  end  
  
  def ocreate_quadruple_build(card_id, position)
    card = $o_player_cards_objs.select{|c| c.id == card_id}.last
    card_index = $o_player_cards_objs.find_index(card)
    card_visual = $o_player_cards[card_index]   
    
    $h_position = []
    
    available_builds = $game.available_builds?(card)
    available_builds.each do |p|
      if p.is_a?(Array)
        p.each{|i| $h_position << i}
      else
        $h_position < i
      end    
    end  
    
    $game.opponent_player.hand.delete($o_player_cards_objs[card_index])    
    $o_player_cards_objs.delete($o_player_cards_objs[card_index])
    $o_player_cards[card_index].delete_from_parent
    $o_player_cards.delete($o_player_cards[card_index])

    $otemp_position = get_first_empty_position
    $game.move_cards_to_position(card, $game.opponent_player, $otemp_position)
    $game.pick_up_build(card, $game.opponent_player, $otemp_position) 

    $h_position.each{|p| clear_position(p)}
    $h_position = []
    show_message("quadruple_build")
    #update_scores
    EM.add_timer 1.8 do
      clear_position(position)
      @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)
      hide_message
      @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
      @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      $turn = true  
      restart_timer
    end
  end     
  
  def create_ultimate_destruction
    mp "User create a ultimate destruction using card #{$object[2].rank} #{$object[2].suit} on position #{$position_i}"
    $game.pick_up_build($object[2], $game.local_player, $position_i)
    add_card_to_pos($object[0], $position_i)
    clear_position($position_i)
     
    $l_player_cards_objs.delete($object[2])
    $l_player_cards.delete($object[0])
    @bcards.delete([$object[0], 'select_card', $object[2]])
    
    App::Persistence["#{$gcm.local_player_id}-udestructions"] += 1
    
    show_flashing_message("ultimate_destruction")
    
    EM.add_timer 1.8 do
      clean_action_menu
      $amenu = []
      hide_message
      EM.add_timer 1.0 do
        next_player_turn
      end    
    end
  end  
  
  def create_destroy
    mp "User create destroy using card #{$object[2].rank} #{$object[2].suit} on position #{$position_i}"
    $game.pick_up_build($object[2], $game.local_player, $position_i)
    add_card_to_pos($object[0], $position_i)
    
    
    $my_turn_actions = "opponent_action:::create_destroy::#{$object[2].id}::#{$position_i}"
    
    clear_position($position_i)
    
    show_flashing_message("destruction")
        
    $l_player_cards_objs.delete($object[2])
    $l_player_cards.delete($object[0])
    @bcards.delete([$object[0], 'select_card', $object[2]])
    
    EM.add_timer 1.8 do
      clean_action_menu
      $amenu = []
      hide_message
      EM.add_timer 1.0 do
        next_player_turn
      end    
    end
  end  
  
  def ocreate_destroy(card_id, position)
    card = $o_player_cards_objs.select{|c| c.id == card_id}.last
    card_index = $o_player_cards_objs.find_index(card)
    card_visual = $o_player_cards[card_index]    
    
    mp "Opponent create new build using card #{card.rank} #{card.suit} on position #{position}"
    $game.pick_up_build(card, $game.opponent_player, position)
    
    move_compu_card(card_visual, $board_positions[position], position, card_id)
    $o_player_cards_objs.delete(card)
    $o_player_cards.delete(card_visual)
    #update_scores
    show_flashing_message("destruction")
    EM.add_timer 1.2 do
      clear_position(position)
      @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)
      hide_message
      @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
      @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      $turn = true
      restart_timer
    end
  end  
  
  def create_stack
    mp "User create a stack using card #{$object[2].rank} #{$object[2].suit} on position #{$position_i}"
    $game.create_stack($object[2], $game.local_player, $position_i)
    add_card_to_pos($object[0], $position_i)
    
    $my_turn_actions = "opponent_action:::create_stack::#{$object[2].id}::#{$position_i}"
    
    clear_position($position_i)
    
    $l_player_cards_objs.delete($object[2])
    $l_player_cards.delete($object[0])
    @bcards.delete([$object[0], 'select_card', $object[2]])
    
    show_flashing_message("stack", $object[2].rank)

    EM.add_timer 1.8 do
      clear_position($position_i)
      clean_action_menu
      $amenu = []
      hide_message
      EM.add_timer 1.0 do
        next_player_turn
      end    
    end    
  end
  
  
  def ocreate_stack(card_id, position)
    card = $o_player_cards_objs.select{|c| c.id == card_id}.last
    card_index = $o_player_cards_objs.find_index(card)
    card_visual = $o_player_cards[card_index]    
    
    NSLog("Opponent create new stack using card #{card.rank} #{card.suit} on position #{position}")
    $game.create_stack(card, $game.opponent_player, position)
    
    move_compu_card(card_visual, $board_positions[position], position, card_id)
    $o_player_cards_objs.delete(card)
    $o_player_cards.delete(card_visual)
    #update_scores
    show_flashing_message("ostack", card.rank)
    EM.add_timer 1.2 do
      clear_position(position)
      @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)
      hide_message
      @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
      @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      $turn = true
      restart_timer
    end
  end    
  
  
  
  def create_steal
    mp "User create a stack using card #{$object[2].rank} #{$object[2].suit} on position #{$position_i}"
    $game.create_stack($object[2], $game.local_player, $position_i)
    add_card_to_pos($object[0], $position_i)
    
    $my_turn_actions = "opponent_action:::create_steal::#{$object[2].id}::#{$position_i}"    
    clear_position($position_i)
    
    show_message("steal")
    
    $l_player_cards_objs.delete($object[2])
    $l_player_cards.delete($object[0])
    @bcards.delete([$object[0], 'select_card', $object[2]])
    
    EM.add_timer 1.8 do
      clear_position($position_i)
      clean_action_menu
      $amenu = []
      hide_message
      EM.add_timer 1.0 do
        next_player_turn
      end    
    end
  end  
  
  def ocreate_steal(card_id, position)
    card = $o_player_cards_objs.select{|c| c.id == card_id}.last
    card_index = $o_player_cards_objs.find_index(card)
    card_visual = $o_player_cards[card_index]    
    
    mp "Opponent create new steal using card #{card.rank} #{card.suit} on position #{position}"
    $game.create_stack(card, $game.opponent_player, position)
    
    move_compu_card(card_visual, $board_positions[position], position, card_id)
    $o_player_cards_objs.delete(card)
    $o_player_cards.delete(card_visual)
    #update_scores
    show_message("osteal")
    EM.add_timer 1.2 do
      clear_position(position)
      @rect2_over1.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.5)
      hide_message
      @rect2_over2.frame = CGRectMake(0, ImageHelper.porcent_for(@screen_height, 100), @screen_width, ImageHelper.porcent_for(@screen_height, 31))
      @rect2_over2.backgroundColor = UIColor.colorWithRed(0/255.0, green:0/255.0, blue:0/255.0, alpha:0.0)
      $turn = true
      restart_timer
    end
  end     
  
  def add_card_to_pos(card, position)
    $positions_cards[position] << card
    
    $positions_cards.each do |p|
      p.each {|c| restore_cards_scale(c)}
    end
    set_card_rotate_for_position(position)
  end  
  
  def get_first_empty_position
    $positions_cards.each_with_index{|p, i| return i if p.size == 0}
  end  
  
  def set_card_rotate_for_position(position)
    rotation = 0

    $positions_cards[position].each_with_index do |c, i|
      c.rotation = rotation
      c.z_index = i + 4
      rotation += 20
      m = c.size.width * 0.12

      defaultx = $positions_cards[position].first.position.x
      defaulty = $positions_cards[position].first.position.y

      c.position = [defaultx + (i*m), defaulty - (i*(m / 2))]    
    end  
  end  
  
  def remove_card_from_board(card)
    card.delete_from_parent
  end
  
  def clear_position(number)
    NSLog("Action: Clear Cards from Position #{number} on board")
    NSLog($positions_cards[number].to_s)
    clean_hightlight_from(number)
    $positions_cards[number].uniq.each do |c| 
      begin 
        c.delete_from_parent if c
      rescue Exception => e
        NSLog(e.message)
      end  
    end  
    $positions_cards[number] = []
    todelete = $game.board_cards.select{|bc| bc.position == number}
    todelete.each{|td| $game.board_cards.delete(td) }
    NSLog("Action: Position #{number} cleared")
  end    

  
end  