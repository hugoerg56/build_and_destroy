class SoundHelper
  
  def self.default_volume
    App::Persistence['volume'] = App::Persistence['volume'] ? App::Persistence['volume'] : 0.1
  end  
  
  def self.toogle_volumen
    if App::Persistence['volume'] == 0.1
      App::Persistence['volume'] = 0.0
    else
      App::Persistence['volume'] = 0.1
    end
  end  
  
  def self.click(loop=false)
    MG::Audio.play('click.mp3', loop, SoundHelper.default_volume)
  end 
  
  def self.main_background_music(loop=false)
    MG::Audio.play('background.mp3', loop, SoundHelper.default_volume) 
  end  
  
  def self.got_item(loop=false)
    MG::Audio.play('gotitem.mp3', loop, SoundHelper.default_volume)
  end
  
end  