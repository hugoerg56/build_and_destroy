$rules1 = "The Deal 
The deck is shuffled, each player is dealt 6 cards the first hand, 5 cards each additional hand thereafter until the deck is complete.  The player’s hands are hidden from each other.

The Play 
The dealer goes last.  The opponent throws off a card to start the game.  Each player takes turns throwing off.  The object is to match cards or build numbers. 

Build and Destroy 
A player can BUILD a number that is in their hand only. The opponent can DESTROY the build if he has the same number. 
"

$rules2 = "Example: 
If there is a 3 on the board and Player 1 has a 2 and a 5, Player 1 can build by putting the 2 down on top of the 3 so Player 2 will know Player 1 is building a 5.  Player 2 has the option to pick up the 5 if he is holding a 5 in his hand.  If the Player 2 is not holding a 5, then the Player 1 can either pick up the 5 that was built, or any other card on the board which he/she is holding.  Player 1 cannot just throw off... he/she will have to pick up either what was built or matches with what is in hand."

$rules3 = "Double Build
Building numbers but in a chain form."

$rules4 = "Example: 
Player 1 build the same 5 by throwing down a 2 on top of the 3 that is on the board.  Player 2 throw off a 4.  Player 1 places an Ace on top of the 4 to create another 5.
"

$rules5 = "Triple Build
The same building concept as the double build just adding a 3rd link to the chain"

$rules6 = "Example: 
Player 1 builds the same 5 by throwing down a 2 on top of the 3 that’s on the board.  Player 2 throws off a 4.  Player 1 places an Ace on top of the 4 to make another 5.  Player 2 throws off another 4.  Player 1 places another Ace creating a third 5.  Player 1 picks up all his/her 5’s which were created.
"

$rules7 = "Quadruple Build
The same building concept as the triple build just adding a 4th link to the chain.

Ultimate Destruction
When either player allows their opponent to Double or Triple build then DESTROYING what was built.

Stacking
Pilling the same number of face cards in one stack.  For the number cards, you will need 3 or more of the same card to stack.  For face cards, you will need all 4 cards to stack.
"

$rules8 = "Example: 
The same 3 is on the board, Player 1 has two 3’s in his/her hand.  Player 2 doesn’t have a 3’s in his/her hand.  Player 1 throws off a 3 during his/her turn until all 3’s are played.  This is when the stacking aspect come in to play.  Player 1 will pick up the stack with the last 3.  Player 1 also has the option to pick up other matches off the board, in between stacking, during his/her turn."

$rules9 = "Stack Steal
When either player allows the opponent to stack, while he/she is picking up the other matching cards off the board, then picks up the stack the opponent just built, during his/her following turn.

Last Hand
The last player to pick up, takes all the remainig cards on the board.
"