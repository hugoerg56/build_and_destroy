class GameHelper
  
  def self.add_background(id, scene)
    bg = MG::Sprite.new(ImageHelper.name_for_phone(id))
    bg.position = [$visible_size.width / 2, $visible_size.height / 2]
    scene.add bg, 0
  end 
  
  def self.add_back_button_to(scene_name, scene, top=2)
    back_button = MG::Sprite.new(ImageHelper.name_for_phone("back-arrow.png"))
    back_button.position = [ImageHelper.left_porcent(6) + back_button.size.width, ImageHelper.top_porcent(top) - back_button.size.height]
    scene.add back_button, 4     
    [back_button, scene_name]
  end
  
  def self.add_title(text, scene)
    title = MG::Text.new(text, 'OpenSans-Regular', 50)
    title.position = [$visible_size.width / 2, ImageHelper.top_porcent(6.5)]
    title.text_color = [0.0,0.0,0.0,1.0]
    scene.add title    
  end  
  
  def self.alert(title, message, button="Ok")
    alert = UIAlertController.alertControllerWithTitle(title, message:message, preferredStyle:UIAlertControllerStyleAlert)
    okButton = UIAlertAction.actionWithTitle(button, style:UIAlertActionStyleDefault, handler:proc{|action|
      puts 'Ok'
    })
    alert.addAction(okButton)
    $root.presentViewController(alert, animated:true, completion:nil)
  end  
  
  def self.match_cards_touch(card, touch)
    if match_touch(card[0], touch)
      game_actions(card[0], card[1], card[2])
      return true
    end 
  end  
  
  def self.card_match_pos(position, touch) 
    if match_touch_pos(position, touch)
      return true
    end  
  end  
  
  def self.match_touch(card, touch)   
    return true if match_x(touch.location.x, card) and match_y(touch.location.y, card)
  end  
  
  def self.match_touch_pos(pos, touch)   
    return true if match_x_pos(touch.location.x, pos) and match_y_pos(touch.location.y, pos)
  end    
  
  def self.game_actions(image, action, game_card)
    self.send(action, image, game_card)
  end  
  
  def self.match_x_pos(x1, pos) 
    start_card_x = pos.position.x
    end_card_x = start_card_x + pos.size.width

    x1 >= start_card_x and x1 <= end_card_x
  end
  
  def self.match_y_pos(y1, pos)
    start_card_y = pos.position.y + pos.size.height
    end_card_y = start_card_y - pos.size.height

    y1 <= start_card_y and y1 >= end_card_y
  end     
  
  def self.match_x(x1, card) 
    scale = scale_of(card)
    start_card_x = (card.position.x - (card.size.width * scale) / 2)
    end_card_x = start_card_x + (card.size.width * scale)

    x1 >= start_card_x and x1 <= end_card_x
  end
  
  def self.match_y(y1, card)
    scale = scale_of(card)
    start_card_y = (card.position.y + ((card.size.height * scale) / 2))
    end_card_y = start_card_y - (card.size.height * scale)

    y1 <= start_card_y and y1 >= end_card_y
  end   
  
  def self.scale_of(card)
    ImageHelper.porcent_for($visible_size.width, 15) / card.size.width
  end 
  
  def self.restore_cards_scale(card)
    $l_player_cards.each{|c| c.scale = scale_of(card)}
  end  
  
  def self.select_card(image, card)
    restore_cards_scale(image)
    image.scale = (scale_of(image) + 0.05)
  end  
  
  def self.move_card_to(card, touch)
    card[0].position = [touch.location.x, touch.location.y]
  end  

  def self.original_position_of(card)
    cards_pos = player_cards_position(74.5)
    i = $l_player_cards.find_index(card)
    [cards_pos[i][0] + card.size.width / 2, (cards_pos[i][1] - card.size.height / 4)]
  end  
  
  def self.player_cards_position(top)
    top = ImageHelper.top_porcent(top)
    initial_left = ImageHelper.left_porcent(1)
    initial_left = ImageHelper.left_porcent(0.2) if $iphone_ver == "iphone5" or $iphone_ver == "iphone4"
    initial_left = ImageHelper.left_porcent(5) if $iphone_ver == "iphone6plus"
    (0..5).each_with_index.map{|n, i| [ initial_left + (ImageHelper.left_porcent(15) * i), top]}
  end  
  
end  