Then(/^the card instance should be a Card class$/) do
  expect(@card.class).to be(Card)
end

Then(/^the cards should have the points of the point structure$/) do
  expect(@card_1.points).to eq(1)
  expect(@card_2.points).to eq(2)
  expect(@card_3.points).to eq(1)
  expect(@card_4.points).to eq(0)
  expect(@card_5.points).to eq(0)
end

Then(/^the cards should have the color of their suit$/) do
  expect(@card_1.color).to eq("red")
  expect(@card_2.color).to eq("red")
  expect(@card_3.color).to eq("black")
  expect(@card_4.color).to eq("black")
end
