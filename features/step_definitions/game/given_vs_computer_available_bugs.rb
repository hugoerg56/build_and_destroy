Before do
  @game = Game.new([{id:'1',name:'rafael camacho',email:'rafael@gmail.com'}, {id:'2',name:'Computer'}])
  @game.players[0] = @game.local_player
  @game.players[1] = @game.opponent_player
end

Given ("a new game, play vs computer to validate available actions for a card") do
  @game.players.map{|player| player.hand=[]}
end

Given (/^local player with cards: (J) of (Diamonds), (\d+) of (Clubs), (\d+) of (Clubs), (A) of (Spades), (A) of (Diamonds), (\d+) of (Clubs)$/) do |card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5, card6, suit6|
  @game.players[0].hand << Card.new(1, card1, suit1)
  @game.players[0].hand << Card.new(1, card2.to_i, suit2)
  @game.players[0].hand << Card.new(1, card3.to_i, suit3)
  @game.players[0].hand << Card.new(1, card4, suit4)
  @game.players[0].hand << Card.new(1, card5, suit5)
  @game.players[0].hand << Card.new(1, card6.to_i, suit6)
end

Given (/^opponent hand with cards: (J) of (Hearts), (\d+) of (Spades), (\d+) of (Clubs), (\d+) of (Hearts), (\d+) of (Hearts), (K) of (Clubs)$/) do |card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5, card6, suit6|
  @game.players[1].hand << Card.new(1, card1, suit1)
  @game.players[1].hand << Card.new(1, card2.to_i, suit2)
  @game.players[1].hand << Card.new(1, card3.to_i, suit3)
  @game.players[1].hand << Card.new(1, card4.to_i, suit4)
  @game.players[1].hand << Card.new(1, card5.to_i, suit5)
  @game.players[1].hand << Card.new(1, card6, suit6)
end
