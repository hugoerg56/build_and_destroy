Then(/^computer (1st|3rd|5th|6th) turn should be able to place a card on position (\d+)$/) do |text, position|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, position.to_i])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Card Placed")
end

Then(/^player (1st|2nd|4th|7th) turn should be able to place a card on position (\d+)$/) do |text,position|
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq("Card Placed")
end

Then(/^computer (2nd|4th) turn should be able to pick up cards on position (\d+)$/) do |text, position|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, position.to_i])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Cards Picked Up")
end

Then(/^player (3rd|5th|6th) turn should be able to pick up cards on position (\d+)$/) do |text,position|
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq([:pick_up])
  expect(@game.pick_up(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq("Cards Picked Up")
end

Then(/^computer 7th turn should be able to place a card on position (\d+)$/) do |position|
  expect(@game.computers_turn).to eq([@game.players[1].hand[1], :place_card, position.to_i])
  expect(@game.place_card(@game.players[1].hand[1], @game.players[1], position.to_i)).to eq("Building Number")
end

Then(/^computer 8th turn should be able to place a card to pick up build on position (\d+)$/) do |position|
  expect(@game.computers_turn).to eq([@game.players[1].hand[1], :pick_up_build, position.to_i])
  expect(@game.pick_up_build(@game.players[1].hand[1], @game.players[1], position.to_i)).to eq("Build")
end

Then("the computer shouldn't be able to create a three cards build") do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Card Placed")

  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Building Number")

  expect(@game.computers_turn).not_to eq([@game.players[1].hand[0], :place_card, 0])
end

Then ("the player shouldnt be able to create a 10 on top of the 8") do
  @game.place_card(@game.players[1].hand[0], @game.players[1], 0)
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 0)).to eq([])
end

Then ("the player should be able to pick up a 10 build") do
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")

  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Building Number")

  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Building Number")

  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 0)).to eq([:pick_up_build])
  expect(@game.pick_up_build(@game.players[1].hand[0], @game.players[1], 0)).to eq("Build")
end
