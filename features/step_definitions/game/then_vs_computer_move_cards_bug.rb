Then(/^vs move card bug, the computer should be able to place a card on position (\d+). (1st|2nd|4th|5th) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, position.to_i])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Card Placed")
end

Then(/^vs move card bug, the player should be able to place a card on position (\d+). (1st|2nd|3rd|4th|5th|6th|7th) turn$/) do |position, text|
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq("Card Placed")
end

Then(/^vs move card bug, the computer should be able to pick up cards on position (\d+). 3rd turn$/) do |position|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, position.to_i])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Cards Picked Up")
end

Then("vs move card bug, the computer should be able to place a card on position 3. 6th turn") do
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 3)).to eq("Card Placed")
end

Then(/^vs move card bug, the computer should be able to place a card on position (\d+). (7th|9th) turn$/) do |position, text|
  expect(@game.place_card(@game.players[1].hand[1], @game.players[1], position.to_i)).to eq("Card Placed")
end

Then("vs move card bug, the computer should be able to build a number on position 6. 8th turn") do
  expect(@game.place_card(@game.players[1].hand[2], @game.players[1], 6)).to eq("Building Number")
end

Then(/^vs move card bug, the player should be able to see the available builds with the board cards$/) do
  expect(@game.available_builds?(@game.players[0].hand[0])).to eq([[12, 2], [13, 7], [1, 4], [8, 9]])
end
