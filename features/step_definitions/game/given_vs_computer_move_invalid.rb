Before do
  @game = Game.new([{id:'1',name:'rafael camacho',email:'rafael@gmail.com'}, {id:'2',name:'Computer'}])
  @game.players[0] = @game.local_player
  @game.players[1] = @game.opponent_player
end

Given ("a new game, play vs computer to validate move to build") do
  @game.players.map{|player| player.hand=[]}
end

Given (/^vs move to build, local player with cards: (A) of (Hearts), (\d+) of (Spades), (\d+) of (Hearts), (\d+) of (Hearts)$/) do |card1, suit1, card2, suit2, card3, suit3, card4, suit4|
  @game.players[0].hand << Card.new(1, card1, suit1)
  @game.players[0].hand << Card.new(1, card2.to_i, suit2)
  @game.players[0].hand << Card.new(1, card3.to_i, suit3)
  @game.players[0].hand << Card.new(1, card4.to_i, suit4)
end

Given (/^vs move to build, opponent hand with cards: (\d+) of (Hearts), (\d+) of (Hearts), (\d+) of (Diamonds), (\d+) of (Clubs)$/) do |card1, suit1, card2, suit2, card3, suit3, card4, suit4|
  @game.players[1].hand << Card.new(1, card1.to_i, suit1)
  @game.players[1].hand << Card.new(1, card2.to_i, suit2)
  @game.players[1].hand << Card.new(1, card3.to_i, suit3)
  @game.players[1].hand << Card.new(1, card4.to_i, suit4)
end
