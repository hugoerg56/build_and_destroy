#MAIN FEATURE
Then(/^the game instance should be a Game class$/) do
  expect(@game.class).to be(Game)
end

Then(/^the game should have players$/) do
  expect(@game.players[0].class).to be(Player)
end

Then(/^the game should have two players$/) do
  expect(@game.players.size).to eq(2)
end

Then(/^the game should have a deck$/) do
  expect(@game.deck.class).to be(Deck)
end

Then(/^the game should have a deck with (-?\d+) remaining cards$/) do |number|
  expect(@game.deck.remaining_cards).to eq(number.to_i)
end

Then(/^the players should have (-?\d+) cards each$/) do |number|
  @game.players.map{|player| expect(player.hand.size).to eq(number.to_i)}
end

# ADD CARD TO BOARD FEATURE
Then(/^the card should be placed to the board$/) do
  expect(@game.board_cards.size).to eq(1)
end

Then(/^the board should have a BoardCard$/) do
  expect(@game.first_board_card_by_position(0).class).to be(BoardCard)
end

Then(/^the board should have a card$/) do
  expect(@game.count_board_cards_by_position(0)).to eq(1)
end

Then(/^the board should have an array with one card$/) do
  expect(@game.board_cards_by_position(0).size).to eq(1)
end

Then(/^the card on the board should be able to be moved$/) do
  @game.move_card(@game.last_card_by_position(0), @game.players[0], 1)
  expect(@game.count_board_cards_by_position(1)).to eq(1)
end

# ADD CARDS PLAY FEATURE
Then(/^it should be able to place a card$/) do
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 0)).to eq([:place_card])
end

Then(/^the computer should be able to place a card$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
end

Then(/^it should return Card Placed$/) do
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Card Placed")
end

Then(/^it should be able to pick up cards$/) do
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 0)).to eq([:pick_up])
end

Then(/^the computer should be able to pick up cards$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, 0])
end

Then(/^it should return Cards Picked Up$/) do
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], 0)).to eq("Cards Picked Up")
end

#EXTRACT A CARD FROM BOARD FEATURE
Then(/^the cards should be removed from the board$/) do
  expect(@game.count_board_cards_by_position(0)).to eq(0)
end

Then(/^the player should have (-?\d+) cards picked up$/) do |number|
  expect(@game.players[1].cards_picked_up.size).to eq(number.to_i)
end

#BUILD NUMBER FEATURE
Then(/^the sum of the cards should be on the players hand$/) do
  expect(@game.build_number(@game.players[1].hand[1], @game.players[1], 0)).to be(true)
end

Then(/^the board should have (-?\d+) cards on the same position$/) do |number|
  expect(@game.count_board_cards_by_position(0)).to eq(number.to_i)
end

#BUILD OR DESTROY FEATURE
Then(/^should be a (Build|Destroy) in the position (-?\d+)$/) do |text, position|
  expect(@game.pick_up_build(@game.players[0].hand[1], @game.players[0], position.to_i)).to eq(text)
end

Then(/^it should be able to create a build$/) do
  expect(@game.available_actions(@game.players[0].hand[2], @game.players[0], 1)).to eq([:pick_up_build])
end

Then(/^the computer should be able to create a destroy$/) do
  expect(@game.available_actions(@game.players[1].hand[2], @game.players[1], 1)).to eq([:create_destroy])
end

Then(/^it should return Build$/) do
  expect(@game.pick_up_build(@game.players[0].hand[2], @game.players[0], 1)).to eq("Build")
end

#STARTING|BUILDING A DOUBLE
Then(/^it should be able to place a card for a new build$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 1)).to eq([:place_card])
end

Then(/^the computer should be able to place a card to start a double$/) do
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 1)).to eq([:place_card])
  expect(@game.computers_turn).to eq([@game.players[1].hand[5], :pick_up_build, 0])
end

Then(/^it should return Card Placed for a new double build$/) do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1)).to eq("Card Placed")
end

Then(/^it should be able to build a new number$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 1)).to eq([:place_card])
end

Then(/^it should return Building Number$/) do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 1)).to eq("Building Number")
end

Then(/^it should be able to move the cards for the double build$/) do
  expect(@game.available_builds?(@game.players[0].hand[1])).to eq([[1], [0]])
  @game.move_cards_to_position(@game.players[0].hand[1], @game.players[0], 0)
end

Then(/^it should be able to create a double build$/) do
  expect(@game.available_actions(@game.players[0].hand[1], @game.players[0], 0)).to eq([:create_double_build])
end

Then(/^it should return Double Build$/) do
  expect(@game.pick_up_build(@game.players[0].hand[1], @game.players[0], 0)).to eq("Double Build")
end

Then(/^it should be able to create an Ultimate Destruction for the double build$/) do
  expect(@game.available_actions(@game.players[1].hand[5], @game.players[1], 0)).to eq([:create_ultimate_destruction])
end

Then(/^the computer should be able to create an Ultimate Destruction for the double build$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[5], :computer_move_to_pick_up_build, 0, [[0], [1]]])
end

Then(/^it should return Ultimate Destruction for the double build$/) do
  expect(@game.pick_up_build(@game.players[1].hand[5], @game.players[1], 0)).to eq("Ultimate Destruction")
end

#STARTING|TRIPLE BUILD
Then(/^it should be able place a card to start a triple build$/) do
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 2)).to eq([:place_card])
end

Then(/^the computer should be able to place a card to start a new build for a triple$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[5], :pick_up_build, 0])
end

Then(/^it should return Card Placed for the triple build to start$/) do
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 2)).to eq("Card Placed")
end

Then(/^it should be able to build a new number for the triple build$/) do
  expect(@game.available_actions(@game.players[1].hand[1], @game.players[1], 2)).to eq([:place_card])
end

Then(/^the computer should be able to build a new number for the triple build$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[4], :computer_move_to_pick_up_build, 0, [[0], [1]]])
end

Then(/^it should return Building Number for the triple build$/) do
  expect(@game.place_card(@game.players[1].hand[1], @game.players[1], 2)).to eq("Building Number")
end

Then(/^it should be able to move the cards for the triple build$/) do
  expect(@game.available_builds?(@game.players[0].hand[1])).to eq([[2], [1], [0]])
  @game.move_cards_to_position(@game.players[0].hand[1], @game.players[0], 0)
end

Then(/^it should be able to create a triple build$/) do
  expect(@game.available_actions(@game.players[0].hand[1], @game.players[0], 0)).to eq([:create_triple_build])
end

Then(/^the play it should be a triple build$/) do
  expect(@game.pick_up_build(@game.players[0].hand[1], @game.players[0], 0)).to eq("Triple Build")
end

Then(/^it should be able to create an Ultimate Destruction for the triple build$/) do
  expect(@game.available_actions(@game.players[1].hand[3], @game.players[1], 0)).to eq([:create_ultimate_destruction])
end

Then(/^the computer should be able to create an Ultimate Destruction for the triple build$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[3], :pick_up_build, 0])
end

Then(/^it should return Ultimate Destruction for the triple build$/) do
  expect(@game.pick_up_build(@game.players[1].hand[3], @game.players[1], 0)).to eq("Ultimate Destruction")
end

#STARTING|QUADRUPLE BUILD
Then(/^it should be able to place a card to start a quadruple build$/) do
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 3)).to eq([:place_card])
end

Then(/^it should return Card Placed for the quadruple build to start$/) do
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 3)).to eq("Card Placed")
end

Then(/^it should be able to build a new number for the quadruple build$/) do
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 3)).to eq([:place_card])
end

Then(/^the computer should be able to build a new number for the quadruple build$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[2], :computer_move_to_pick_up_build, 0, [[0], [1], [2]]])
end

Then(/^it should return Building Number for the quadruple build$/) do
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 4)).to eq("Card Placed")
end

Then(/^it should be able to move the cards for the quadruple build$/) do
  expect(@game.available_builds?(@game.players[0].hand[1])).to eq([[2], [1], [0], [3, 4]])
end

Then(/^the play it should be a quadruple build$/) do
  expect(@game.move_to_start_build([3,4], @game.players[0])).to eq([[3, 4], 5])
  expect(@game.move_to_pick_up_build([0, 1, 2, 5], @game.players[0].hand[1], @game.players[0])).to eq([[0, 1, 2, 5], 3, "Quadruple Build"])
end

Then(/^the player should be able to create a quadruple build$/) do
  expect(@game.available_actions(@game.players[0].hand[1], @game.players[0], 0)).to eq([:create_quadruple_build])
end

Then(/^the computer should be able to create an Ultimate Destruction for the quadruple build$/) do
  expect(@game.available_actions(@game.players[1].hand[1], @game.players[1], 0)).to eq([:create_ultimate_destruction])
  expect(@game.computers_turn).to eq([@game.players[1].hand[1], :pick_up_build, 0])
end

Then(/^it should return Ultimate Destruction for the quadruple build$/) do
  expect(@game.pick_up_build(@game.players[1].hand[1], @game.players[1], 0)).to eq("Ultimate Destruction")
end

Then(/^the local player should have the Achievement Deluxe Devastation count increased$/) do
  expect(@game.local_player.achievements.select{|a| a if a.name == "Deluxe Devastation"}.first.count).to eq(1)
end

#STACK A NUMBER FEATURE
Then(/^it should be able to Stack a Number$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:pick_up, :place_card])
end

Then(/^it should return Stacking Number$/) do
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Stacking Number")
end

Then(/^it should be able to create a Stack$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:create_stack, :place_card])
end

Then(/^it should return Stack$/) do
  expect(@game.create_stack(@game.players[0].hand[0], @game.players[0], 0)).to eq("Stack")
end

Then(/^it should be able to stack a number for facecard (2nd card)$/) do |text|
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 1)).to eq([:pick_up, :place_card])
end

Then(/^it should be able to stack a number for facecard (3rd card)$/) do |text|
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 1)).to eq([:place_card])
end

Then(/^the computer should be able to stack a number for facecard (2nd card)$/) do |text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, 1])
end

Then(/^the computer should be able to stack a number for facecard (3rd card)$/) do |text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 1])
end

Then(/^it should return Stacking Number for facecard (2nd card|3rd card)$/) do |text|
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 1)).to eq("Stacking Number")
end

Then(/^it should be able to create a Stack for facecard$/) do
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 1)).to eq([:create_stack])
end

Then(/^the computer should be able to create a Stack for facecard$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :create_stack, 1])
end

Then(/^it should return Stack for facecard$/) do
  expect(@game.create_stack(@game.players[1].hand[0], @game.players[1], 1)).to eq("Stack")
end

Then(/^it should be able to create a Steal$/) do
  expect(@game.available_actions(@game.players[1].hand[3], @game.players[1], 0)).to eq([:create_steal])
end

Then(/^the computer should be able to create a Steal$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[3], :create_stack, 0])
end

Then(/^it should return Steal$/) do
  expect(@game.create_stack(@game.players[1].hand[3], @game.players[1], 0)).to eq("Steal")
end

Then(/^it should return Steal from the other player$/) do
  expect(@game.create_stack(@game.players[0].hand[1], @game.players[0], 1)).to eq("Steal")
end

Then(/^it should be no winner, and the score should be the next one$/) do |data|
  values = data.rows_hash

  expect(@game.players[0].round_score).to eq(values[:p0_round_score].to_i)
  expect(@game.players[0].total_score).to eq(values[:p0_total_score].to_i)
  expect(@game.players[1].round_score).to eq(values[:p1_round_score].to_i)
  expect(@game.players[1].total_score).to eq(values[:p1_total_score].to_i)
end

#KEEP BOARD CARDS FEATURE
Then(/^the last player should have all the cards that where in the board$/) do
  expect(@game.players[0].count_cards_picked_up).to eq(2)
  expect(@game.players[1].count_cards_picked_up).to eq(8)
end

Then(/^the winner should be the local player rafael$/) do
  expect(@game.players[1].name).to eq("rafael camacho")
end

#NEW ROUND FEATURE
Then(/^the players should have no cards picked up$/) do
  @game.players.map{|player| expect(player.count_cards_picked_up).to eq(0)}
end

Then(/^the players should have no points$/) do
  @game.players.map{|player| expect(player.points).to eq(0)}
end

Then(/^the board should have no cards$/) do
  expect(@game.board_cards.count).to eq(0)
end

Then(/^the local player should be the 1st player$/) do
  expect(@game.players.first.name).to eq(@game.local_player.name)
end

Then(/^the opponent player should be the 1st player$/) do
  expect(@game.players.first.name).to eq(@game.opponent_player.name)
end

#MOVE STACK CARDS
Then(/^it should be able to move the cards to create a stack with facecards$/) do
  expect(@game.available_stacks?(@game.players[0].hand[0])).to eq([0, 1, 2])
  @game.move_stack_cards_to_position(@game.players[0].hand[0], @game.players[0], 0)
end

Then(/^it should be able create a stack with the moved facecards$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:create_stack])
  expect(@game.create_stack(@game.players[0].hand[0], @game.players[0], 0)).to eq("Stack")
end

Then(/^it should be able to move the cards to create a stack with number cards$/) do
  expect(@game.available_stacks?(@game.players[1].hand[0])).to eq([3, 4, 5])
  @game.move_stack_cards_to_position(@game.players[1].hand[0], @game.players[1], 3)
end

Then(/^it should be able create a stack with the moved number cards$/) do
  expect(@game.available_actions(@game.players[1].hand[0], @game.players[1], 3)).to eq([:create_stack])
  expect(@game.create_stack(@game.players[1].hand[0], @game.players[1], 3)).to eq("Stack")
end

#FACECARDS STACK FOUR CARDS VALIDATION
Then(/^the player should be able only to pick_up$/) do
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:pick_up])
end

Then(/^the player should be able to place a card to stack by moving the cards on the board$/) do
  @game.move_stack_cards_to_position(@game.players[0].hand[0], @game.players[0], 0)
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], 0)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], 0)).to eq("Stacking Number")
end

Then(/^the player should be able to create a stack with the moved cards$/) do
  expect(@game.create_stack(@game.players[0].hand[0], @game.players[0], 0)).to eq("Stack")
end

#TRASH CAN FEATURE
Then(/^the local player should get the trash can achievement$/) do
  expect(@game.any_achievements?).to eq(@game.local_player.achievements.select{|a| a if a.name == "Trash Can"})
  expect(@game.local_player.coins).to eq(250)
  expect(@game.local_player.achievements.select{|a| a if a.name == "Addiction 1"}.first.count).to eq(10)
  expect(@game.local_player.achievements.select{|a| a if a.name == "Addiction 2"}.first.count).to eq(10)
end

Then(/^the local player should get the card hog and master builder achievement$/) do
  expect(@game.any_achievements?).to eq(@game.local_player.achievements.select{|a| a if a.name == "Card Hog" || a.name == "Master Builder" })
  expect(@game.local_player.coins).to eq(1500)
end

Then(/^the computer should be able to build another number on top of the first build with an Ace$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  @game.players[1].drop_card(@game.players[1].hand[0])
end

Then(/^the computer should be able to build another number on top of the first build with a 2 of Spades$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, 0])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], 0)).to eq("Building Number")
end

Then(/^the available build should be a build$/) do
  expect(@game.available_builds?(@game.players[0].hand[0])).to eq([[0], [1]])
  expect(@game.move_to_pick_up_build([0, 1], @game.players[0].hand[0], @game.players[0])).to eq([[0, 1], 2, "Build"])
end

Then(/^the two cards should be a build$/) do
  expect(@game.move_to_start_build([0,1], @game.players[0])).to eq([[0,1],2])
end
