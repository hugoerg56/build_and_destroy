Before do
  @game = Game.new([{id:'1',name:'rafael camacho',email:'rafael@gmail.com'}, {id:'2',name:'Computer'}])
  @game.players[0] = @game.local_player
  @game.players[1] = @game.opponent_player
end

Given ("a new game, play vs computer to validate a three cards build") do
  @game.players.map{|player| player.hand=[]}
end

Given (/^opponent hand with cards: (\d+) of (Clubs), (J) of (Hearts), (\d+) of (Spades), (\d+) of (Diamonds), (Q) of (Diamonds) and (\d+) of (Clubs)$/) do |card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5, card6, suit6|
  @game.players[1].hand << Card.new(1, card1.to_i, suit1)
  @game.players[1].hand << Card.new(1, card2, suit2)
  @game.players[1].hand << Card.new(1, card3.to_i, suit3)
  @game.players[1].hand << Card.new(1, card4.to_i, suit4)
  @game.players[1].hand << Card.new(1, card5, suit5)
  @game.players[1].hand << Card.new(1, card6.to_i, suit6)
end

Given (/^local player with cards: (J) of (Clubs), (\d+) of (Spades), (\d+) of (Hearts), (\d+) of (Clubs), (Q) of (Hearts), (\d+) of (Diamonds)$/) do |card1, suit1, card2, suit2, card3, suit3, card4, suit4, card5, suit5, card6, suit6|
  @game.players[0].hand << Card.new(1, card1, suit1)
  @game.players[0].hand << Card.new(1, card2.to_i, suit2)
  @game.players[0].hand << Card.new(1, card3.to_i, suit3)
  @game.players[0].hand << Card.new(1, card4.to_i, suit4)
  @game.players[0].hand << Card.new(1, card5, suit5)
  @game.players[0].hand << Card.new(1, card6.to_i, suit6)
end

Given(/^opponent hand with cards: (A) of (Spades), (\d+) of (Hearts) and (\d+) of (Diamonds)$/) do |card1, suit1, card2, suit2, card3, suit3|
  @game.players[1].hand << Card.new(1, card1, suit1)
  @game.players[1].hand << Card.new(1, card2.to_i, suit2)
  @game.players[1].hand << Card.new(1, card3.to_i, suit3)
end

Given(/^local player with cards: (\d+) of (Clubs)$/) do |card1, suit1|
  @game.players[0].hand << Card.new(1, card1.to_i, suit1)
end

Given("opponent hand with cards in a vs computer play check the three cards build") do
  @game.players[1].hand << Card.new(1, "A", "Hearts")
  @game.players[1].hand << Card.new(1, 4, "Hearts")
  @game.players[1].hand << Card.new(1, 10, "Hearts")
  @game.players[1].hand << Card.new(1, 6, "Hearts")
end

Given("local player with cards in a vs computer play check the three cards build") do
  @game.players[0].hand << Card.new(1, 5, "Hearts")
end

Given ("an 8 build the player shouldnt be able to create a 10 with a 2 of spades") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 5, "Diamonds")
  @game.players[1].hand << Card.new(1, 3, "Diamonds")

  @game.players[1].hand << Card.new(1, 2, "Spades")
  @game.players[1].hand << Card.new(1, 10, "Clubs")
end

Given ("an 8 build the player should be able to create a 10 with a 2 of spades") do
  @game.players.map{|player| player.hand=[]}

  @game.players[1].hand << Card.new(1, 5, "Hearts")
  @game.players[1].hand << Card.new(1, 3, "Diamonds")

  @game.players[1].hand << Card.new(1, 2, "Spades")
  @game.players[1].hand << Card.new(1, 10, "Clubs")
  @game.players[1].hand << Card.new(1, 8, "Clubs")
end
