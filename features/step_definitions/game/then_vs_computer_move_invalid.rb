Then(/^vs move to build, the player should be able to place a card on position (\d+). (1st|2nd|3rd|4th) turn$/) do |position, text|
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq("Card Placed")
end

Then(/^vs move to build, the computer should be able to build a number on position (\d+). (1st|3rd) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, position.to_i])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Building Number")
end

Then(/^vs move to build, the computer should be able to pick up a build on position (\d+). (2nd) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up_build, position.to_i])
  expect(@game.pick_up_build(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Build")
end

Then(/^vs move to build, the computer should be able to move to pick up cards on position 12. 4th turn$/) do
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :computer_move_to_pick_up_build, 11, [[11], [12]]])
  expect(@game.computer_move_to_pick_up_build(@game.players[1].hand[0], @game.players[1], 11)).to eq("Build")
end
