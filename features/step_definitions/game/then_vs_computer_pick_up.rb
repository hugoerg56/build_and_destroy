Then(/^vs pick ups bug, the player should be able to place a card on position (\d+). (1st|4th|5th|6th|8th|10th) turn$/) do |position, text|
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq("Card Placed")
end

Then(/^vs pick ups bug, the computer should be able to place a card on position (\d+). (2nd|3rd|4th) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, position.to_i])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Card Placed")
end


Then(/^vs pick ups bug, the computer should be able to pick up a build by moving cards on position (\d+). Positions (\d+) and (\d+) for (5th|6th) turn$/) do |position, pos1, pos2, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :computer_move_to_pick_up_build, position.to_i, [[pos1.to_i, pos2.to_i]]])
end

Then(/^vs pick ups bug, the computer should be able to pick up a build by moving cards on position 2. Positions (\d+), (\d+), (\d+) for (8th|9th) turn$/) do |pos1, pos2, pos3, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[@game.players[1].hand.count-1], :computer_move_to_pick_up_build, 1, [[pos1.to_i, pos2.to_i], [pos3.to_i]]])
end

Then(/^vs pick ups bug, the computer should be able to place a card on position (\d+). (5th|6th|8th|9th) turn$/) do |position, text|
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Card Placed")
end

Then(/^vs pick ups bug, the player should be able to build a number on position (\d+). (7th) turn$/) do |position, text|
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq([:place_card])
  expect(@game.place_card(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq("Building Number")
end

Then(/^vs pick ups bug, the computer should be able to build a number on position (\d+). (1st|3rd) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :place_card, position.to_i])
  expect(@game.place_card(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Building Number")
end

Then(/^vs pick ups bug, the player should be able to create a Destroy on position (\d+). (2nd) turn$/) do |position, text|
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq([:create_destroy])
  expect(@game.pick_up_build(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq("Destroy")
end

Then(/^vs pick ups bug, the computer should be able to create a Destroy on position (\d+). (7th) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up_build, position.to_i])
  expect(@game.pick_up_build(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Destroy")
end

Then(/^vs pick ups bug, the player should be able to pick up cards on position (\d+). (3rd|9th) turn$/) do |position, text|
  expect(@game.available_actions(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq([:pick_up])
  expect(@game.pick_up(@game.players[0].hand[0], @game.players[0], position.to_i)).to eq("Cards Picked Up")
end

Then(/^vs pick ups bug, the computer should be able to pick up cards on position (\d+). (4th) turn$/) do |position, text|
  expect(@game.computers_turn).to eq([@game.players[1].hand[0], :pick_up, position.to_i])
  expect(@game.pick_up(@game.players[1].hand[0], @game.players[1], position.to_i)).to eq("Cards Picked Up")
end

Then("vs pick ups bug, the computer shouldn't be able to place a card on position 5. 10th turn") do
  expect(@game.computers_turn).not_to eq([@game.players[1].hand[0], :place_card, 5])
end
