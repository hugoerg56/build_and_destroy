Then(/^the deck instance should be a Deck class$/) do
  expect(@deck.class).to be(Deck)
end

Then(/^the deck should have one less card$/) do
  expect(@deck.remaining_cards).to eq(51)
end
