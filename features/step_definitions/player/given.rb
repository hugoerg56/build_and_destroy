Before do
  @player = Player.new({'id'=>'1','name'=>'rafael camacho','email'=>'rafael@gmail.com'})
end

#MAIN FEATURE
Given ("a new player instance") do
end

Given ("select a card to throw it in the board") do
  @player.hand=[1,2,3,4,5,6]
end

#POINT STRUCTURE FEATURE
Given ("some cards picked up") do
  @player.cards_picked_up = [BoardCard.new(Card.new(1, "4", "Diamonds"), @player, 0, 0), BoardCard.new(Card.new(2, "A", "Diamonds"), @player, 0, 0), BoardCard.new(Card.new(3, "2", "Spades"), @player, 0, 0), BoardCard.new(Card.new(4, "10", "Spades"), @player, 0, 0), BoardCard.new(Card.new(5, "Q", "Hearts"), @player, 0, 0), BoardCard.new(Card.new(6, "7", "Spades"), @player, 0, 0), BoardCard.new(Card.new(4, "A", "Spades"), @player, 0, 0), BoardCard.new(Card.new(4, "J", "Spades"), @player, 0, 0), BoardCard.new(Card.new(4, "Q", "Spades"), @player, 0, 0), BoardCard.new(Card.new(4, "K", "Spades"), @player, 0, 0), BoardCard.new(Card.new(1, "A", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "2", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "3", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "4", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "5", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "6", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "7", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "8", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "9", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "10", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "J", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "Q", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(1, "K", "Clubs"), @player, 0, 0), BoardCard.new(Card.new(5, "10", "Hearts"), @player, 0, 0), BoardCard.new(Card.new(5, "J", "Hearts"), @player, 0, 0), BoardCard.new(Card.new(5, "K", "Hearts"), @player, 0, 0), BoardCard.new(Card.new(5, "A", "Hearts"), @player, 0, 0)]
end
