Feature: Building a number with the cards on the board or with the cards on the players hand

  Background:
    Given two cards to build a number
    And a builded number, extract build

  Scenario: Validating a number to build
    Then the cards should be removed from the board
    Then the player should have 3 cards picked up
