Feature: Creating a double build with the cards on the board or with the cards on the players hand

  Background:
    Given a build, start a second build

  Scenario: Validating a double build
    Then it should be able to place a card for a new build
    Then the computer should be able to place a card to start a double
    Then it should return Card Placed for a new double build
