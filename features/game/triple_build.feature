Feature: Creating a triple build with the cards on the board or with the cards on the players hand

  Background:
    Given a build, start a second build
    And a double, start a third build
    And a new card, build a triple

  Scenario: Validating a triple build
    Then it should be able to build a new number for the triple build
    Then the computer should be able to build a new number for the triple build
    Then it should return Building Number for the triple build
    Then it should be able to move the cards for the triple build
    Then it should be able to create a triple build
    Then the play it should be a triple build
