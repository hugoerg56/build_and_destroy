Feature: Validating a new round

  Scenario: Validating game instance
    Given a last play
    Then the opponent player should be the 1st player
    Given a new round
    Then the game should have a deck with 40 remaining cards
    Then the players should have 6 cards each
    Then the players should have no cards picked up
    Then the players should have no points
    Then the board should have no cards
    Then the local player should be the 1st player
