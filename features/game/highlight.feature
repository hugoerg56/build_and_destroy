Feature: Validating the players highlighted cards to create builds

  Background: Validating the players highlighted cards to create builds

  Scenario: checking the highlighted posible builds
    Given some separated 8 builds on the board and a 4 and 8 in the players hand
    Then the player should have the posibility to create a 4 or an 8
    Then the player should be able to get the cards grouped on the board
    Then the player should be able to create a build with the previous highlighted cards

  Scenario: checking 9 build with highlighted cards fix
    Given cards to create a build highlighting cards for a 9
    Then the player should be able to create a build given highlighting cards for a 9

  Scenario: checking 10 build with highlighted cards fix
    Given cards to create a build highlighting cards for a 10
    Then the player should be able to create a build given highlighting cards for a 10

  Scenario: checking 8 build with highlighted cards fix
    Given cards to create a build highlighting cards for a 8
    Then the player should be able to create a build given highlighting cards for a 8

  Scenario: Validating move to pick up build bug scenario
    Given a 9 build on the board move to pick up the 9
    Then the player should be able to pick up the 9 build

  Scenario: Validating move to pick up 9s build scenario
    Given a 9 build with 9 on the board
    Then the player should be able to pick up the 9 build and the 9 on the board

  Scenario: Validating move to pick up 9s build scenario
    Given two 9 builds with 5 cards pick up the 9s
    Then the player should be able to pick up the 9s
