Feature: Creating the trash can achievement

  Scenario: Validating the trash can achievement
    Given 10 games with 0 points
    Then the local player should get the trash can achievement
