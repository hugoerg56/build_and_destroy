Feature: Creating an Ultimate Destruction with the cards on the board or with the cards on the players hand

  Background:
    Given a build, start a second build
    And a new card, build a double

  Scenario: Validating an ultimate destruction for the double build
    Then it should return Building Number
    Then the computer should be able to create an Ultimate Destruction for the double build
    Then it should be able to move the cards for the double build
    Then it should be able to create an Ultimate Destruction for the double build
    Then it should return Ultimate Destruction for the double build
