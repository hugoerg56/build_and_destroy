Feature: Validating the optional cards on a build

  Scenario: Validating action
    Given a build of the player
    Then the computer should be able to build another number on top of the first build with an Ace
    Then the computer should be able to build another number on top of the first build with a 2 of Spades
