Feature: Validating actions of the computer in a vs play

    Scenario: Validating the move to build of the computer
    Given a new game, play vs computer to validate move to build

    Given vs move to build, local player with cards: A of Hearts, 5 of Spades, 8 of Hearts, 2 of Hearts
    Given vs move to build, opponent hand with cards: 6 of Hearts, 7 of Hearts, 3 of Diamonds, 8 of Clubs

    Then vs move to build, the player should be able to place a card on position 11. 1st turn
    Then vs move to build, the computer should be able to build a number on position 11. 1st turn

    Then vs move to build, the player should be able to place a card on position 12. 2nd turn
    Then vs move to build, the computer should be able to pick up a build on position 11. 2nd turn

    Then vs move to build, the player should be able to place a card on position 11. 3rd turn
    Then vs move to build, the computer should be able to build a number on position 12. 3rd turn

    Then vs move to build, the player should be able to place a card on position 14. 4th turn
    Then vs move to build, the computer should be able to move to pick up cards on position 12. 4th turn
