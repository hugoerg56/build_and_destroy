Feature: Building a number with the cards on the board or with the cards on the players hand

  Background:
    Given two cards to build a number
    And two builded numbers by each players

  Scenario: Validating if it's a build or a destroy
    Then should be a Build in the position 0
    Then should be a Destroy in the position 1
