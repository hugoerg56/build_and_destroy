Feature: Validating the deal of cards

  Scenario: Validating the deal of cards at a new turn
    Given a new turn deal cards
    Then the game should have a deck with 30 remaining cards
    Then the players should have 5 cards each
