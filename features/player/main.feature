Feature: Registrating a new Player

  Background:
    Given a new player instance
    Given select a card to throw it in the board

  Scenario: Validating player instance
    Then the player instance should be a Player class
    Then the selected card should be extracted from the players hand
    Then the player should have 9 new achivements with 0 count
