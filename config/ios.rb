# -*- coding: utf-8 -*-
require 'bubble-wrap'
require 'bubble-wrap/reactor'
require 'bubble-wrap/location'
require 'bubble-wrap/mail'
require 'motion_print'
require 'motion-social'
require 'sugarcube-nsdata'
require 'sugarcube-animations'
require 'helu'


Motion::Project::App.setup do |app|

  # Use `rake ios:config' to see complete project settings.
  app.name = 'Build And Destroy'
  app.version = "0.0.59"
  app.short_version = "1.0"
  app.device_family = [:iphone]
  app.identifier = "sweet-shops-games.build-and-destroy"  

  app.sdk_version = "10.1"
  app.deployment_target = "8.4"
  app.icons = ["Icon-60.png", "Icon-60@2x.png", "Icon-60@3x.png"]
  
  
  #DEVELOP
  app.codesign_certificate = "iPhone Developer: Hugo Rincon Gonzalez (Q4K322EFD8)"
  app.provisioning_profile = './provisioning/development.mobileprovision'
  
  #RELEASE
  #app.codesign_certificate = "iPhone Distribution: Sweet Shops Games, LLC (EF87D8MCA2)"
  #app.provisioning_profile = './provisioning/production.mobileprovision'
  
  app.frameworks += %w(GameKit SystemConfiguration Security Social Accounts)
  #app.entitlements['get-task-allow'] = false
  
  app.interface_orientations = [:portrait]  
  app.info_plist['UISupportedInterfaceOrientations'] = ['UIInterfaceOrientationPortrait']
  app.fonts = ['OpenSans-Regular.ttf', 'OpenSans-Bold.ttf', 'OpenSans-Light.ttf']
  
  app.vendor_project('vendor/HockeySDK.framework', :static, :products => ['HockeySDK'], :headers_dir => 'Headers')
  
end
